package com.weayrihs.moviesapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.weayrihs.moviesapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MoviesApp)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}