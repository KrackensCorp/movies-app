package com.weayrihs.moviesapp.presentation

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.weayrihs.moviesapp.R
import com.weayrihs.moviesapp.data.dto.MovieList
import com.weayrihs.moviesapp.data.dto.Results

class Adapter(data: MutableList<Results>) : BaseQuickAdapter<Results, BaseViewHolder>(R.layout.item_movie_reviews_list, data),
    LoadMoreModule {

    override fun convert(holder: BaseViewHolder, item: Results) {
        holder.setText(R.id.title, item.openingDate)
            .setText(R.id.summaryShort, item.summaryShort)

        val imageView = holder.getView<ImageView>(R.id.imageView)
        val context = holder.itemView.context

        Glide.with(context)
            .load(item.multimedia!!.src)
            .into(imageView)
    }
}