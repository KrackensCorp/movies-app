package com.weayrihs.moviesapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.weayrihs.moviesapp.R
import com.weayrihs.moviesapp.data.Repository
import com.weayrihs.moviesapp.databinding.FragmentMovieReviewsListBinding
import java.lang.Exception

class MovieReviewsListFragment : Fragment(R.layout.fragment_movie_reviews_list) {

    private lateinit var binding: FragmentMovieReviewsListBinding
    private val adapter = Adapter(mutableListOf())
    private val repository = Repository()
    private val customLoadMoreView = LoadMoreView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieReviewsListBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerViewMyTaskFragment.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        initAdapter()
    }

    private fun initAdapter() {
        binding.recyclerViewMyTaskFragment.adapter = adapter
        adapter.loadMoreModule.loadMoreView = customLoadMoreView
        adapter.loadMoreModule.setOnLoadMoreListener { loadMore() }
        adapter.loadMoreModule.isAutoLoadMore = true
        adapter.setDiffCallback(DiffCallback())
        adapter.setOnItemChildClickListener { _, view, position ->

        }
        val data = repository.firstPage()
        adapter.setNewInstance(data)
    }

    private fun loadMore() {
        try {
            val data = repository.nextPage()
            adapter.addData(data)
            adapter.loadMoreModule.isEnableLoadMore = true
            adapter.loadMoreModule.loadMoreComplete()
            if (repository.isEnd()) {
                adapter.loadMoreModule.loadMoreEnd()
            }
        } catch (e: Exception) {
            adapter.loadMoreModule.loadMoreFail()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        repository.reset()
    }
}