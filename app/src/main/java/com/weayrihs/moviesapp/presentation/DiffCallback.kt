package com.weayrihs.moviesapp.presentation

import androidx.recyclerview.widget.DiffUtil
import com.weayrihs.moviesapp.data.dto.Results

class DiffCallback : DiffUtil.ItemCallback<Results>() {
        override fun areItemsTheSame(oldItem: Results, newItem: Results): Boolean =
            oldItem.summaryShort == newItem.summaryShort

        override fun areContentsTheSame(oldItem: Results, newItem: Results): Boolean =
            oldItem == newItem
    }