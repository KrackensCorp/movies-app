package com.weayrihs.moviesapp.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.weayrihs.moviesapp.data.api.RetroInstance
import com.weayrihs.moviesapp.data.api.RetroService
import com.weayrihs.moviesapp.data.dto.MovieList
import com.weayrihs.moviesapp.data.dto.Results

//class Repository {
//    private var listItems: MutableList<Results> = mutableListOf()
//    // надо наполнить список из https://api.nytimes.com/svc/movies/v2/reviews/all.json?offset=100&order=by-opening-openingDate&api-key=ux8dGRIRU1n2eXF5gi8ga3gFiMKuluJt
//    fun getAllNotImaged() = listItems
//
//
//
//}

import com.weayrihs.moviesapp.data.dto.Multimedia
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random

class Repository {
//    private var listImagedItems: MutableList<Results> = mutableListOf()
    private var pageInfo = PageInfo()
    private val mayException = true // false чтобы не вызывать случайным образом ошибки подргузки
    private var listImagedItems: MutableList<Results> = mutableListOf()



    init {
        reset()
    }

    fun reset() {
        initImagedData()
        //initImagedRemoteData()
        pageInfo = PageInfo()
    }

    private fun initImagedRemoteData() {
        val retroInstance = RetroInstance.getRetroInstance().create(RetroService::class.java)
        val call = retroInstance.getReviews()
        Log.e("TAG", call.toString())

//        call.enqueue(object : Callback<List<Results>> {
//            override fun onFailure(call: Call<List<Results>>, t: Throwable) {
//                recyclerListData.postValue(null)
//            }
//
//            override fun onResponse(call: Call<List<Results>>, response: Response<List<Results>>) {
//                if (response.isSuccessful) {
//                    recyclerListData.postValue(response.body())
//                } else  {
//                    recyclerListData.postValue(null)
//                }
//            }
//        })


//        listImagedRemoteItems.add(
//            Results(
//                summaryShort = "getReviews()",
//                multimedia = Multimedia(
//                    src = "https://sun2.ufanet.userapi.com/s/v1/ig2/MmH5LDVS7DNIyyeL2C-yo8UwDrXee1HwfgqEPB0x7e_DkNdz6619RQ-wJd-I1VlePnr1p6BqkY4GLnktiUpgLOQL.jpg?size=50x50&quality=96&crop=188,407,1144,1144&ava=1"
//                )
//            )
//        )
    }

    private fun initImagedData() {
        listImagedItems.add(
            Results(
                summaryShort = "Самое новое уведомление! Спасибо что ты с нами, дальше будет интерено(но это не точно, а может быть точно, но я не уверен)",
                multimedia = Multimedia(
                src = "https://sun2.ufanet.userapi.com/s/v1/ig2/MmH5LDVS7DNIyyeL2C-yo8UwDrXee1HwfgqEPB0x7e_DkNdz6619RQ-wJd-I1VlePnr1p6BqkY4GLnktiUpgLOQL.jpg?size=50x50&quality=96&crop=188,407,1144,1144&ava=1"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "Просто какой то тескт, чтобы просто посмотреть. Вообще часто мы занимаемся полной ерундой, написание этого теста не исключение",
                multimedia = Multimedia(
                src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTU9_876g2xpvgbasrtxnfckj_tC9it0jWoMw&usqp=CAU"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "Вот представь:  — У тебя есть 1000 рублей... Или, для круглого счета, пусть у тебя 1024 гыгыгыгыгыгыгыгыгыгыгыгыг",
                multimedia = Multimedia(
                src = "https://image.flaticon.com/icons/png/512/185/185034.png"
                )
            )
        )

        listImagedItems.add(
            Results(
                summaryShort = "Программист ставит себе на тумбочку перед сном два стакана. Один с водой - на случай, если захочет ночью пить. А второй пустой - на случай, если не захочет.",
                multimedia = Multimedia(
                src = "https://image.flaticon.com/icons/png/512/185/185034.png"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "Если вы посмотрите на код, который вы писали более полугода назад, то, скорей всего, вам покажется, что автор – кто-то другой.",
                multimedia = Multimedia(
                src = "https://previews.123rf.com/images/businessvector/businessvector1510/businessvector151000024/45788264-newspaper-icon.jpg"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "На свете существует 10 типов людей: те, кто понимает двоичную систему счисления, и те, кто не понимает",
                multimedia = Multimedia(
                src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTU9_876g2xpvgbasrtxnfckj_tC9it0jWoMw&usqp=CAU"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "Отладка – это удаление из программного кода различных багов. Значит, программирование – это создание и добавление этих самых багов в код?",
                multimedia = Multimedia(
                src = "https://image.flaticon.com/icons/png/512/185/185034.png"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "Мне лень вспомнить шутки, поэтому дальше просто какой то текст",
                multimedia = Multimedia(
                src = "https://previews.123rf.com/images/businessvector/businessvector1510/businessvector151000024/45788264-newspaper-icon.jpg"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "тут тоже какой то текст, мне уже лень даже поднимать первую букву в верхний регистр",
                multimedia = Multimedia(
                src = "https://previews.123rf.com/images/businessvector/businessvector1510/businessvector151000024/45788264-newspaper-icon.jpg"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "осталось немного элементов, я еще держусь и я точно знаю что дойду до конца",
                multimedia = Multimedia(
                src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTU9_876g2xpvgbasrtxnfckj_tC9it0jWoMw&usqp=CAU"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "давайте споём, тулула тутутутулула в голове моей замкнуло лалалаааааааа",
                multimedia = Multimedia(
                src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTU9_876g2xpvgbasrtxnfckj_tC9it0jWoMw&usqp=CAU"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "на поле танки грохотали, солдаты шли в последний бой, а молодого командира несли с пробитой головой",
                multimedia = Multimedia(
                src = "https://img.freepik.com/free-vector/note-music-logo-design_93835-645.jpg?size=338&ext=jpg"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "под танк ударила болванка, прощай, гардейский экипаж! четыре трупа возле танка дополнят утренний пезаж",
                multimedia = Multimedia(
                src = "https://img.freepik.com/free-vector/note-music-logo-design_93835-645.jpg?size=338&ext=jpg"
                )
            )
        )
        listImagedItems.add(
            Results(
                summaryShort = "последнее уведомление! Ура ура ура, я это сделал. Наверно это было самое сложное в этом примере",
                multimedia = Multimedia(
                src = "https://previews.123rf.com/images/businessvector/businessvector1510/businessvector151000024/45788264-newspaper-icon.jpg"
                )
            )
        )
    }

    fun firstPage(): MutableList<Results> {
        pageInfo.nextPage()
        var firstIndex = pageInfo.page * pageInfo.COUNT_ON_PAGE
        var endIndex = firstIndex + pageInfo.COUNT_ON_PAGE - 1

        if (firstIndex > listImagedItems.size - 1 || endIndex > listImagedItems.size - 1) {
            pageInfo.isFinish = true
            if (firstIndex > listImagedItems.size - 1)
                firstIndex = listImagedItems.size - 1
            if (endIndex > listImagedItems.size - 1)
                endIndex = listImagedItems.size - 1
        }

        return listImagedItems.slice(firstIndex..endIndex).toMutableList()
    }

    fun nextPage(): MutableList<Results> {
        if (mayException) {
            if (Random.nextBoolean())
                throw AnyException()
        }
        if (pageInfo.isFinish) return mutableListOf()
        pageInfo.nextPage()
        var firstIndex = pageInfo.page * pageInfo.COUNT_ON_PAGE
        var endIndex = firstIndex + pageInfo.COUNT_ON_PAGE - 1

        if (firstIndex > listImagedItems.size - 1 || endIndex > listImagedItems.size - 1) {
            pageInfo.isFinish = true
            if (firstIndex > listImagedItems.size - 1) firstIndex = listImagedItems.size - 1
            if (endIndex > listImagedItems.size - 1) endIndex = listImagedItems.size - 1
        }

        return listImagedItems.slice(firstIndex..endIndex).toMutableList()
    }

    fun isEnd() = pageInfo.isFinish

    /** class for save page info*/
    inner class PageInfo(var page: Int = -1) {
        val COUNT_ON_PAGE = 5
        var isFinish = false
        fun nextPage() {
            page += 1
        }
    }
}