package com.weayrihs.moviesapp.data.api

import com.weayrihs.moviesapp.data.dto.MovieList
import com.weayrihs.moviesapp.data.dto.Results
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RetroService {

    @GET("reviews/all.json?offset=100&order=by-opening-date&api-key=ux8dGRIRU1n2eXF5gi8ga3gFiMKuluJt")
    @Headers("Accept:application/json")

    fun getReviews() : Call<Results>

}