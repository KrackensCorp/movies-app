package com.weayrihs.moviesapp.data.dto

import com.google.gson.annotations.SerializedName

data class MovieList(
    @SerializedName("status")
    var status: String? = null,

    @SerializedName("copyright")
    var copyright: String? = null,

    @SerializedName("has_more")
    var hasMore: Boolean? = null,

    @SerializedName("num_results")
    var numResults: Int? = null,

    @SerializedName("results")
    var results: ArrayList<Results> = arrayListOf()
)